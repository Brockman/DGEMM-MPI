# Parallel Matrix Multiplication
_HPC LEAP student's project by [A. Herten](mailto:a.herten@fz-juelich.de) (2016)_

Two means of parallelism are used:

* OpenMP on a per-node-basis
* MPI across nodes, using Cannon's algorithm to create tiles

Look at the Handout PDF in the `Materials` directory for further directions.
