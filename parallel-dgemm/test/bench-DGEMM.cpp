#include <gtest/gtest.h>
#include "../src/gemm.hpp"
#include <chrono>
#include <omp.h>

int main(void) {

    int m;
    int n;
    int k;
    double alpha {1.0};
    double beta {1.0};
    int lda;
    int ldb;
    int ldc;

    srand48(100);

    double t1;
    double t2;

    for(int i = 128;i <= 1024; i+=128) {
        n = i;
        m = n;
        k = n;
        lda = n;
        ldb = n;
        ldc = n;

        double *A = new double[n*n];
        double *B = new double[n*n];
        double *C = new double[n*n]; 
        double *correctC = new double[n*n];
        for (int i = 0; i < n*n; i++) {
            A[i] = drand48();
            B[i] = drand48();
            C[i] = drand48();
            correctC[i] = C[i];
        }
        t1 = omp_get_wtime();
        DGEMM(m, n, k, alpha, A,lda, B, ldb, beta, correctC, ldc);
        t2 = omp_get_wtime();

        std::cout << "size: " << m << "\ntime:" << t2 - t1 << std::endl << std::endl;
    }
}
